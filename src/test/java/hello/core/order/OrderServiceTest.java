package hello.core.order;

import hello.core.member.Grade;
import hello.core.member.Member;
import hello.core.member.MemberService;
import hello.core.member.MemberServiceImpl;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

public class OrderServiceTest {
    MemberService memberService = new MemberServiceImpl();
    OrderService orderService = new OrderServiceImpl();

    @Test
    void createOrder() {
        //given
        Long memberId = 1L;
        Member member = new Member(memberId, "jy", Grade.VIP);
        memberService.join(member);
        //when
        Order order = orderService.createOrder(memberId, "book", 50000);
        Assertions.assertThat(order.getDiscountPrice()).isEqualTo(1000);
        //then
    }

}
